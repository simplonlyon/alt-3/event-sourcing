package co.simplon.alt3.eventsourcing.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import co.simplon.alt3.eventsourcing.event.DomainEvent;

@Repository
public interface EventRepository  extends MongoRepository<DomainEvent,String>{
    List<DomainEvent> findByUuid(String uuid);
    
}
