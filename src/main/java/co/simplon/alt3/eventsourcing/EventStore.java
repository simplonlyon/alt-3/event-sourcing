package co.simplon.alt3.eventsourcing;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.simplon.alt3.eventsourcing.event.DomainEvent;
import co.simplon.alt3.eventsourcing.event.IPublisher;
import co.simplon.alt3.eventsourcing.repository.EventRepository;

@Service
public class EventStore implements IPublisher {


@Autowired
private EventRepository eventRepo;


    public void save(DomainEvent event) {
        eventRepo.save(event);

    }

    @Override
    public void publish(DomainEvent event) {
        save(event);

    }
}
