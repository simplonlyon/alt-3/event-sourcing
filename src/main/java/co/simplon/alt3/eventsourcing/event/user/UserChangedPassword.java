package co.simplon.alt3.eventsourcing.event.user;

import java.util.UUID;

import co.simplon.alt3.eventsourcing.event.DomainEvent;

public class UserChangedPassword extends DomainEvent<User> {
    
    private String newPassword;
    public UserChangedPassword(UUID uuid, String newPassword) {
        super(uuid);
        this.newPassword = newPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }
    
}
