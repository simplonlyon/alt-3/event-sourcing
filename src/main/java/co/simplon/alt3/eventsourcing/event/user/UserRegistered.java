package co.simplon.alt3.eventsourcing.event.user;

import java.util.UUID;

import co.simplon.alt3.eventsourcing.event.DomainEvent;

public class UserRegistered extends DomainEvent<User>{
    private String email;
    private String username;
    private String password;
    private String role;
    public UserRegistered(UUID uuid, String email, String username, String password, String role) {
        super(uuid);
        this.email = email;
        this.username = username;
        this.password = password;
        this.role = role;
    }
    public String getEmail() {
        return email;
    }
    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }
    public String getRole() {
        return role;
    }
    
}
