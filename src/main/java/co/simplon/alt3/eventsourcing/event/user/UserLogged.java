package co.simplon.alt3.eventsourcing.event.user;

import java.util.UUID;

import co.simplon.alt3.eventsourcing.event.DomainEvent;

public class UserLogged extends DomainEvent<User> {
    
    private String ip;

    public UserLogged(UUID uuid, String ip) {
        super(uuid);
        this.ip = ip;
    }

    public String getIp() {
        return ip;
    }

}
