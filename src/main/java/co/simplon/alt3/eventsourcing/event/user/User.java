package co.simplon.alt3.eventsourcing.event.user;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import co.simplon.alt3.eventsourcing.event.DomainEvent;
import co.simplon.alt3.eventsourcing.event.IPublisher;

public class User {
    private UUID uuid;
    private boolean activated = true;
    private int failedLogins = 0;
    private String password;
    private List<String> oldPasswords = new ArrayList<>();

    public User(List<DomainEvent<User>> events) {
        for (DomainEvent<User> domainEvent : events) {
            apply(domainEvent);
        }
    }

    private void apply(DomainEvent<User> domainEvent) {
        if (domainEvent.getClass() == UserRegistered.class) {
            UserRegistered event = (UserRegistered) domainEvent;
            uuid = event.getUuid();
            password = event.getPassword();
            oldPasswords.add(password);
            return;
        }
        if (domainEvent.getClass() == UserChangedPassword.class) {
            UserChangedPassword event = (UserChangedPassword) domainEvent;
            password = event.getNewPassword();
            oldPasswords.add(password);
        }
        if (domainEvent.getClass() == UserDeactivated.class) {
            if (System.currentTimeMillis() - domainEvent.getTimestamp() < 900000) {
                activated = false;
            }
            return;
        }
        if (domainEvent.getClass() == UserLogFailed.class) {
            failedLogins++;
            return;
        }
        if (domainEvent.getClass() == UserLogged.class) {
            failedLogins = 0;
            return;
        }

    }

    public static UUID register(String email, String username, String password, IPublisher publisher) {
        if (!validate(email) || !validate(username) || !validate(password)) {
            return null;
        }

        UUID uuid = UUID.randomUUID();
        UserRegistered registered = new UserRegistered(uuid, email, username, password, "ROLE_USER");
        publisher.publish(registered);
        return uuid;
    }

    public void login(String password, String ip, IPublisher publisher) {
        if (!activated) {
            return;
        }
        if (password == this.password) {
            publisher.publish(new UserLogged(uuid, ip));
        } else {
            publisher.publish(new UserLogFailed(uuid, ip));
            if (failedLogins == 2) {
                publisher.publish(new UserDeactivated(uuid));
            }
        }
    }


    public void changePassword(String oldPassword, String newPassword, IPublisher publisher) {
        if(newPassword != oldPassword || oldPasswords.contains(newPassword)) {
            return;
        }
        publisher.publish(new UserChangedPassword(uuid, newPassword));

    }

    private static boolean validate(String toValidate) {
        return true;
    }

}
