package co.simplon.alt3.eventsourcing.event;

public interface IPublisher {
    void publish(DomainEvent event);
}
