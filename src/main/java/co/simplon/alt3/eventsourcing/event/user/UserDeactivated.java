package co.simplon.alt3.eventsourcing.event.user;

import java.util.UUID;

import co.simplon.alt3.eventsourcing.event.DomainEvent;

public class UserDeactivated extends DomainEvent<User> {

    public UserDeactivated(UUID uuid) {
        super(uuid);
    }
    
}
