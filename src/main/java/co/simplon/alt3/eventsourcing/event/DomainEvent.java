package co.simplon.alt3.eventsourcing.event;

import java.util.UUID;

public abstract class DomainEvent<T> {
    private UUID uuid;
    public UUID getUuid() {
        return uuid;
    }


    protected Long timestamp = System.currentTimeMillis();
    

    public DomainEvent(UUID uuid) {
        this.uuid = uuid;
    }


    public Long getTimestamp() {
        return timestamp;
    }
}
