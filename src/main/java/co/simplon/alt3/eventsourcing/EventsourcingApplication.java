package co.simplon.alt3.eventsourcing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EventsourcingApplication {

	public static void main(String[] args) {
		System.out.println(System.currentTimeMillis());

		SpringApplication.run(EventsourcingApplication.class, args);
	}

}
