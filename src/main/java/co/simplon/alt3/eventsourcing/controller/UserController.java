package co.simplon.alt3.eventsourcing.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.alt3.eventsourcing.EventStore;
import co.simplon.alt3.eventsourcing.event.IPublisher;
import co.simplon.alt3.eventsourcing.event.user.User;
import co.simplon.alt3.eventsourcing.repository.EventRepository;

@RestController
@RequestMapping("/api/user")
public class UserController {
    @Autowired
    private EventStore publisher;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void save(@RequestBody RegisterDTO register) {

        User.register(register.email, register.username, register.password, publisher);
    }

}
